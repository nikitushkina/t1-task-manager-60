package ru.t1.nikitushkina.tm.dto.response;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import ru.t1.nikitushkina.tm.dto.model.UserDTO;

@NoArgsConstructor
public class UserRemoveResponse extends AbstractUserResponse {

    public UserRemoveResponse(@Nullable UserDTO user) {
        super(user);
    }

}
