package ru.t1.nikitushkina.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.t1.nikitushkina.tm.api.endpoint.*;

public interface IServiceLocator {

    @NotNull
    IAuthEndpoint getAuthEndpointClient();

    @NotNull
    ILoggerService getLoggerService();

    @NotNull
    IProjectEndpoint getProjectEndpointClient();

    @NotNull
    IPropertyService getPropertyService();

    @NotNull
    ISystemEndpoint getSystemEndpointClient();

    @NotNull
    ITaskEndpoint getTaskEndpointClient();

    @NotNull
    ITokenService getTokenService();

    @NotNull
    IUserEndpoint getUserEndpointClient();

}
