package ru.t1.nikitushkina.tm.listener.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.nikitushkina.tm.dto.model.TaskDTO;
import ru.t1.nikitushkina.tm.dto.request.TaskListRequest;
import ru.t1.nikitushkina.tm.dto.response.TaskListResponse;
import ru.t1.nikitushkina.tm.enumerated.Sort;
import ru.t1.nikitushkina.tm.event.ConsoleEvent;
import ru.t1.nikitushkina.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

@Component
public final class TaskListListener extends AbstractTaskListener {

    @NotNull
    public static final String NAME = "task-list";

    @NotNull
    public static final String DESCRIPTION = "Display all tasks.";

    @Override
    @EventListener(condition = "@taskListListener.getName() == #consoleEvent.name")
    public void execute(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("[TASK LIST]");
        System.out.println("ENTER SORT:");
        System.out.println(Arrays.toString(Sort.values()));
        @NotNull final String sortType = TerminalUtil.nextLine();
        @NotNull final Sort sort = Sort.toSort(sortType);
        @Nullable final TaskListRequest request = new TaskListRequest(getToken());
        request.setSort(sort);
        @Nullable final TaskListResponse response = taskEndpoint.listTask(request);
        @NotNull final List<TaskDTO> tasks = response.getTasks();
        renderTasks(tasks);
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}
