package ru.t1.nikitushkina.tm.service;

import liquibase.Liquibase;
import liquibase.exception.LiquibaseException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.t1.nikitushkina.tm.api.service.dto.IProjectDTOService;
import ru.t1.nikitushkina.tm.api.service.dto.ITaskDTOService;
import ru.t1.nikitushkina.tm.api.service.dto.IUserDTOService;
import ru.t1.nikitushkina.tm.comparator.NameComparator;
import ru.t1.nikitushkina.tm.configuration.ServerConfiguration;
import ru.t1.nikitushkina.tm.dto.model.TaskDTO;
import ru.t1.nikitushkina.tm.dto.model.UserDTO;
import ru.t1.nikitushkina.tm.enumerated.Sort;
import ru.t1.nikitushkina.tm.enumerated.Status;
import ru.t1.nikitushkina.tm.exception.field.DescriptionEmptyException;
import ru.t1.nikitushkina.tm.exception.field.IdEmptyException;
import ru.t1.nikitushkina.tm.exception.field.NameEmptyException;
import ru.t1.nikitushkina.tm.exception.field.StatusEmptyException;
import ru.t1.nikitushkina.tm.exception.user.UserIdEmptyException;
import ru.t1.nikitushkina.tm.marker.UnitCategory;
import ru.t1.nikitushkina.tm.migration.AbstractSchemeTest;

import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import static ru.t1.nikitushkina.tm.constant.ProjectTestData.USER_PROJECT1;
import static ru.t1.nikitushkina.tm.constant.ProjectTestData.USER_PROJECT2;
import static ru.t1.nikitushkina.tm.constant.TaskTestData.*;
import static ru.t1.nikitushkina.tm.constant.UserTestData.*;

@Category(UnitCategory.class)
public final class TaskServiceTest extends AbstractSchemeTest {

    @Nullable
    private static IProjectDTOService PROJECT_SERVICE;

    @Nullable
    private static ITaskDTOService SERVICE;

    @Nullable
    private static IUserDTOService USER_SERVICE;

    @NotNull
    private static String userId = "";

    @BeforeClass
    public static void setUp() throws Exception {
        final Liquibase liquibase = liquibase("changelog/changelog-master.xml");
        liquibase.dropAll();
        liquibase.update("scheme");

        @NotNull final ApplicationContext context = new AnnotationConfigApplicationContext(ServerConfiguration.class);
        PROJECT_SERVICE = context.getBean(IProjectDTOService.class);
        SERVICE = context.getBean(ITaskDTOService.class);
        USER_SERVICE = context.getBean(IUserDTOService.class);
        @NotNull final UserDTO user = USER_SERVICE.create(USER_TEST_LOGIN, USER_TEST_PASSWORD);
        userId = user.getId();
    }

    @AfterClass
    public static void tearDown() throws Exception {
        USER_SERVICE.clear();
    }

    @Test
    public void addByUserId() throws Exception {
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            SERVICE.add("", USER_TASK3);
        });
        Assert.assertNotNull(SERVICE.add(userId, USER_TASK3));
        @Nullable final TaskDTO task = SERVICE.findOneById(userId, USER_TASK3.getId());
        Assert.assertNotNull(task);
        Assert.assertEquals(USER_TASK3.getId(), task.getId());
    }

    @Test
    public void changeTaskStatusById() throws Exception {
        @NotNull final Status status = Status.COMPLETED;
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            SERVICE.changeTaskStatusById(null, USER_TASK1.getId(), status);
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            SERVICE.changeTaskStatusById("", USER_TASK1.getId(), status);
        });
        Assert.assertThrows(IdEmptyException.class, () -> {
            SERVICE.changeTaskStatusById(userId, null, status);
        });
        Assert.assertThrows(IdEmptyException.class, () -> {
            SERVICE.changeTaskStatusById(userId, "", status);
        });
        Assert.assertThrows(StatusEmptyException.class, () -> {
            SERVICE.changeTaskStatusById(userId, USER_TASK1.getId(), null);
        });
        SERVICE.changeTaskStatusById(userId, USER_TASK1.getId(), status);
        @Nullable final TaskDTO task = SERVICE.findOneById(userId, USER_TASK1.getId());
        Assert.assertNotNull(task);
        Assert.assertEquals(status, task.getStatus());
    }

    @After
    public void clean() throws Exception {
        SERVICE.clear(userId);
        PROJECT_SERVICE.clear(userId);
    }

    @Test
    public void clearByUserId() throws Exception {
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            SERVICE.clear("");
        });
        SERVICE.clear(userId);
        Assert.assertEquals(0, SERVICE.getSize(userId));
    }

    @Test
    public void create() throws Exception {
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            SERVICE.create(null, USER_TASK3.getName());
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            SERVICE.create("", USER_TASK3.getName());
        });
        Assert.assertThrows(NameEmptyException.class, () -> {
            SERVICE.create(userId, null);
        });
        Assert.assertThrows(NameEmptyException.class, () -> {
            SERVICE.create(userId, "");
        });
        @NotNull final TaskDTO task = SERVICE.create(userId, USER_TASK3.getName());
        Assert.assertEquals(task.getId(), SERVICE.findOneById(userId, task.getId()).getId());
        Assert.assertEquals(USER_TASK3.getName(), task.getName());
        Assert.assertEquals(userId, task.getUserId());
    }

    @Test
    public void createWithDescription() throws Exception {
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            SERVICE.create(null, USER_TASK3.getName(), USER_TASK3.getDescription());
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            SERVICE.create("", USER_TASK3.getName(), USER_TASK3.getDescription());
        });
        Assert.assertThrows(NameEmptyException.class, () -> {
            SERVICE.create(userId, null, USER_TASK3.getDescription());
        });
        Assert.assertThrows(NameEmptyException.class, () -> {
            SERVICE.create(userId, "", USER_TASK3.getDescription());
        });
        Assert.assertThrows(DescriptionEmptyException.class, () -> {
            SERVICE.create(userId, USER_TASK3.getName(), null);
        });
        Assert.assertThrows(DescriptionEmptyException.class, () -> {
            SERVICE.create(userId, USER_TASK3.getName(), "");
        });
        @NotNull final TaskDTO task = SERVICE.create(userId, USER_TASK3.getName(), USER_TASK3.getDescription());
        Assert.assertEquals(task.getId(), SERVICE.findOneById(userId, task.getId()).getId());
        Assert.assertEquals(USER_TASK3.getName(), task.getName());
        Assert.assertEquals(USER_TASK3.getDescription(), task.getDescription());
        Assert.assertEquals(userId, task.getUserId());
    }

    @Test
    public void existsById() throws Exception {
        Assert.assertFalse(SERVICE.existsById(userId, ""));
        Assert.assertFalse(SERVICE.existsById(userId, NON_EXISTING_TASK_ID));
        Assert.assertTrue(SERVICE.existsById(userId, USER_TASK1.getId()));
    }

    @Test
    public void existsByIdByUserId() throws Exception {
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            SERVICE.existsById("", NON_EXISTING_TASK_ID);
        });
        Assert.assertFalse(SERVICE.existsById(userId, ""));
        Assert.assertFalse(SERVICE.existsById(userId, NON_EXISTING_TASK_ID));
        Assert.assertTrue(SERVICE.existsById(userId, USER_TASK1.getId()));
    }

    @Test
    public void findAllByProjectId() throws Exception {
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            @NotNull final Collection<TaskDTO> testCollection = SERVICE.findAllByProjectId(null, USER_PROJECT1.getId());
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            @NotNull final Collection<TaskDTO> testCollection = SERVICE.findAllByProjectId("", USER_PROJECT1.getId());
        });
        @NotNull final Collection<TaskDTO> emptyCollection = Collections.emptyList();
        Assert.assertEquals(emptyCollection, SERVICE.findAllByProjectId(USER_TEST.getId(), null));
        Assert.assertEquals(emptyCollection, SERVICE.findAllByProjectId(USER_TEST.getId(), ""));
        final List<TaskDTO> tasks = SERVICE.findAllByProjectId(userId, USER_PROJECT1.getId());
        Assert.assertEquals(2, tasks.size());
        tasks.forEach(task -> Assert.assertEquals(userId, task.getUserId()));
        tasks.forEach(task -> Assert.assertEquals(USER_PROJECT1.getId(), task.getProjectId()));
    }

    @Test
    public void findAllByUserId() throws Exception {
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            SERVICE.findAll("");
        });
        final List<TaskDTO> tasks = SERVICE.findAll(userId);
        Assert.assertEquals(2, tasks.size());
        tasks.forEach(task -> Assert.assertEquals(userId, task.getUserId()));
    }

    @Test
    public void findAllComparatorByUserId() throws Exception {
        @Nullable Comparator comparator = null;
        Assert.assertNotNull(SERVICE.findAll(userId, comparator));
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            @Nullable Comparator comparatorInner = null;
            SERVICE.findAll("", comparatorInner);
        });
        comparator = NameComparator.INSTANCE;
        final List<TaskDTO> tasks = SERVICE.findAll(userId, comparator);
        tasks.forEach(task -> Assert.assertEquals(userId, task.getUserId()));
    }

    @Test
    public void findAllSortByUserId() throws Exception {
        @Nullable Sort sort = null;
        Assert.assertNotNull(SERVICE.findAll(userId, sort));
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            @Nullable Sort sortInner = null;
            SERVICE.findAll("", sortInner);
        });
        sort = Sort.BY_NAME;
        final List<TaskDTO> tasks = SERVICE.findAll(userId, sort);
        tasks.forEach(task -> Assert.assertEquals(userId, task.getUserId()));
    }

    @Test
    public void findOneById() throws Exception {
        Assert.assertThrows(IdEmptyException.class, () -> {
            SERVICE.findOneById(userId, "");
        });
        Assert.assertNull(SERVICE.findOneById(userId, NON_EXISTING_TASK_ID));
        @Nullable final TaskDTO task = SERVICE.findOneById(userId, USER_TASK1.getId());
        Assert.assertNotNull(task);
        Assert.assertEquals(USER_TASK1.getId(), task.getId());
    }

    @Test
    public void findOneByIdByUserId() throws Exception {
        Assert.assertThrows(IdEmptyException.class, () -> {
            SERVICE.findOneById(userId, "");
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            SERVICE.existsById("", USER_TASK1.getId());
        });
        Assert.assertNull(SERVICE.findOneById(userId, NON_EXISTING_TASK_ID));
        @Nullable final TaskDTO task = SERVICE.findOneById(userId, USER_TASK1.getId());
        Assert.assertNotNull(task);
        Assert.assertEquals(USER_TASK1.getId(), task.getId());
    }

    @Test
    public void getSizeByUserId() throws Exception {
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            SERVICE.getSize("");
        });
        Assert.assertEquals(2, SERVICE.getSize(userId));
    }

    @Before
    public void initTest() throws Exception {
        PROJECT_SERVICE.add(userId, USER_PROJECT1);
        PROJECT_SERVICE.add(userId, USER_PROJECT2);
        SERVICE.add(userId, USER_TASK1);
        SERVICE.add(userId, USER_TASK2);
    }

    @Test
    public void removeByIdByUserId() throws Exception {
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            SERVICE.removeById(null, null);
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            SERVICE.removeById("", null);
        });
        Assert.assertThrows(IdEmptyException.class, () -> {
            SERVICE.removeById(userId, null);
        });
        Assert.assertThrows(IdEmptyException.class, () -> {
            SERVICE.removeById(userId, "");
        });
        SERVICE.removeById(userId, USER_TASK2.getId());
        Assert.assertNull(SERVICE.findOneById(userId, USER_TASK2.getId()));
    }

    @Test
    public void removeByUserId() throws Exception {
        SERVICE.remove(userId, USER_TASK2);
        Assert.assertNull(SERVICE.findOneById(userId, USER_TASK2.getId()));
    }

    @Test
    public void updateById() throws Exception {
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            SERVICE.updateById(null, USER_TASK1.getId(), USER_TASK1.getName(), USER_TASK1.getDescription());
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            SERVICE.updateById("", USER_TASK1.getId(), USER_TASK1.getName(), USER_TASK1.getDescription());
        });
        Assert.assertThrows(IdEmptyException.class, () -> {
            SERVICE.updateById(userId, null, USER_TASK1.getName(), USER_TASK1.getDescription());
        });
        Assert.assertThrows(IdEmptyException.class, () -> {
            SERVICE.updateById(userId, "", USER_TASK1.getName(), USER_TASK1.getDescription());
        });
        Assert.assertThrows(NameEmptyException.class, () -> {
            SERVICE.updateById(userId, USER_TASK1.getId(), null, USER_TASK1.getDescription());
        });
        Assert.assertThrows(NameEmptyException.class, () -> {
            SERVICE.updateById(userId, USER_TASK1.getId(), "", USER_TASK1.getDescription());
        });
        Assert.assertThrows(DescriptionEmptyException.class, () -> {
            SERVICE.updateById(userId, USER_TASK1.getId(), USER_TASK1.getName(), null);
        });
        Assert.assertThrows(DescriptionEmptyException.class, () -> {
            SERVICE.updateById(userId, USER_TASK1.getId(), USER_TASK1.getName(), "");
        });
        @NotNull final String name = USER_TASK1.getName() + NON_EXISTING_TASK_ID;
        @NotNull final String description = USER_TASK1.getDescription() + NON_EXISTING_TASK_ID;
        SERVICE.updateById(userId, USER_TASK1.getId(), name, description);
        @Nullable final TaskDTO task = SERVICE.findOneById(userId, USER_TASK1.getId());
        Assert.assertNotNull(task);
        Assert.assertEquals(name, task.getName());
        Assert.assertEquals(description, task.getDescription());
    }

}
