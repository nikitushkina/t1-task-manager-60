package ru.t1.nikitushkina.tm.service;

import liquibase.Liquibase;
import liquibase.exception.LiquibaseException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.t1.nikitushkina.tm.api.service.dto.IProjectDTOService;
import ru.t1.nikitushkina.tm.api.service.dto.IUserDTOService;
import ru.t1.nikitushkina.tm.comparator.NameComparator;
import ru.t1.nikitushkina.tm.configuration.ServerConfiguration;
import ru.t1.nikitushkina.tm.dto.model.ProjectDTO;
import ru.t1.nikitushkina.tm.dto.model.UserDTO;
import ru.t1.nikitushkina.tm.enumerated.Sort;
import ru.t1.nikitushkina.tm.enumerated.Status;
import ru.t1.nikitushkina.tm.exception.field.DescriptionEmptyException;
import ru.t1.nikitushkina.tm.exception.field.IdEmptyException;
import ru.t1.nikitushkina.tm.exception.field.NameEmptyException;
import ru.t1.nikitushkina.tm.exception.field.StatusEmptyException;
import ru.t1.nikitushkina.tm.exception.user.UserIdEmptyException;
import ru.t1.nikitushkina.tm.marker.UnitCategory;
import ru.t1.nikitushkina.tm.migration.AbstractSchemeTest;

import java.util.Comparator;
import java.util.List;

import static ru.t1.nikitushkina.tm.constant.ProjectTestData.*;
import static ru.t1.nikitushkina.tm.constant.UserTestData.USER_TEST_LOGIN;
import static ru.t1.nikitushkina.tm.constant.UserTestData.USER_TEST_PASSWORD;

@Category(UnitCategory.class)
public final class ProjectServiceTest extends AbstractSchemeTest {

    @Nullable
    private static IProjectDTOService SERVICE;

    @Nullable
    private static IUserDTOService USER_SERVICE;

    @NotNull
    private static String userId = "";

    @BeforeClass
    public static void setUp() throws Exception {
        final Liquibase liquibase = liquibase("changelog/changelog-master.xml");
        liquibase.dropAll();
        liquibase.update("scheme");
        @NotNull final ApplicationContext context = new AnnotationConfigApplicationContext(ServerConfiguration.class);
        SERVICE = context.getBean(IProjectDTOService.class);
        USER_SERVICE = context.getBean(IUserDTOService.class);
        @NotNull final UserDTO user = USER_SERVICE.create(USER_TEST_LOGIN, USER_TEST_PASSWORD);
        userId = user.getId();
    }

    @AfterClass
    public static void tearDown() throws Exception {
        USER_SERVICE.clear();
    }

    @Test
    public void addByUserId() throws Exception {
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            SERVICE.add(null, USER_PROJECT3);
        });
        Assert.assertNotNull(SERVICE.add(userId, USER_PROJECT3));
        @Nullable final ProjectDTO project = SERVICE.findOneById(userId, USER_PROJECT3.getId());
        Assert.assertNotNull(project);
        Assert.assertEquals(USER_PROJECT3.getId(), project.getId());
    }

    @Test
    public void changeProjectStatusById() throws Exception {
        @NotNull final Status status = Status.COMPLETED;
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            SERVICE.changeProjectStatusById(null, USER_PROJECT1.getId(), status);
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            SERVICE.changeProjectStatusById("", USER_PROJECT1.getId(), status);
        });
        Assert.assertThrows(IdEmptyException.class, () -> {
            SERVICE.changeProjectStatusById(userId, null, status);
        });
        Assert.assertThrows(IdEmptyException.class, () -> {
            SERVICE.changeProjectStatusById(userId, "", status);
        });
        Assert.assertThrows(StatusEmptyException.class, () -> {
            SERVICE.changeProjectStatusById(userId, USER_PROJECT1.getId(), null);
        });
        SERVICE.changeProjectStatusById(userId, USER_PROJECT1.getId(), status);
        @Nullable final ProjectDTO project = SERVICE.findOneById(userId, USER_PROJECT1.getId());
        Assert.assertNotNull(project);
        Assert.assertEquals(status, project.getStatus());
    }

    @After
    public void clean() throws Exception {
        SERVICE.clear(userId);
    }

    @Test
    public void clearByUserId() throws Exception {
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            SERVICE.clear("");
        });
        SERVICE.clear(userId);
        Assert.assertEquals(0, SERVICE.getSize(userId));
    }

    @Test
    public void create() throws Exception {
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            SERVICE.create(null, USER_PROJECT3.getName());
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            SERVICE.create("", USER_PROJECT3.getName());
        });
        Assert.assertThrows(NameEmptyException.class, () -> {
            SERVICE.create(userId, null);
        });
        Assert.assertThrows(NameEmptyException.class, () -> {
            SERVICE.create(userId, "");
        });
        @NotNull final ProjectDTO project = SERVICE.create(userId, USER_PROJECT3.getName());
        Assert.assertEquals(project.getId(), SERVICE.findOneById(userId, project.getId()).getId());
        Assert.assertEquals(USER_PROJECT3.getName(), project.getName());
        Assert.assertEquals(userId, project.getUserId());
    }

    @Test
    public void createWithDescription() throws Exception {
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            SERVICE.create(null, USER_PROJECT3.getName(), USER_PROJECT3.getDescription());
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            SERVICE.create("", USER_PROJECT3.getName(), USER_PROJECT3.getDescription());
        });
        Assert.assertThrows(NameEmptyException.class, () -> {
            SERVICE.create(userId, null, USER_PROJECT3.getDescription());
        });
        Assert.assertThrows(NameEmptyException.class, () -> {
            SERVICE.create(userId, "", USER_PROJECT3.getDescription());
        });
        Assert.assertThrows(DescriptionEmptyException.class, () -> {
            SERVICE.create(userId, USER_PROJECT3.getName(), null);
        });
        Assert.assertThrows(DescriptionEmptyException.class, () -> {
            SERVICE.create(userId, USER_PROJECT3.getName(), "");
        });
        @NotNull final ProjectDTO project = SERVICE.create(userId, USER_PROJECT3.getName(), USER_PROJECT3.getDescription());
        Assert.assertEquals(project.getId(), SERVICE.findOneById(userId, project.getId()).getId());
        Assert.assertEquals(USER_PROJECT3.getName(), project.getName());
        Assert.assertEquals(USER_PROJECT3.getDescription(), project.getDescription());
        Assert.assertEquals(userId, project.getUserId());
    }

    @Test
    public void existsById() throws Exception {
        Assert.assertThrows(UserIdEmptyException.class, () -> SERVICE.existsById(null, NON_EXISTING_PROJECT_ID));
        Assert.assertThrows(UserIdEmptyException.class, () -> SERVICE.existsById("", NON_EXISTING_PROJECT_ID));
        Assert.assertFalse(SERVICE.existsById(userId, ""));
        Assert.assertFalse(SERVICE.existsById(userId, NON_EXISTING_PROJECT_ID));
        Assert.assertTrue(SERVICE.existsById(userId, USER_PROJECT1.getId()));
    }

    @Test
    public void existsByIdByUserId() throws Exception {
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            SERVICE.existsById(null, NON_EXISTING_PROJECT_ID);
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            SERVICE.existsById("", NON_EXISTING_PROJECT_ID);
        });
        Assert.assertFalse(SERVICE.existsById(userId, ""));
        Assert.assertFalse(SERVICE.existsById(userId, NON_EXISTING_PROJECT_ID));
        Assert.assertTrue(SERVICE.existsById(userId, USER_PROJECT1.getId()));
    }

    @Test
    public void findAllByUserId() throws Exception {
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            SERVICE.findAll("");
        });
        final List<ProjectDTO> projects = SERVICE.findAll(userId);
        Assert.assertEquals(2, projects.size());
        projects.forEach(project -> Assert.assertEquals(userId, project.getUserId()));
    }

    @Test
    public void findAllComparatorByUserId() throws Exception {
        @Nullable Comparator comparator = null;
        Assert.assertNotNull(SERVICE.findAll(userId, comparator));
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            @Nullable Comparator comparatorInner = null;
            SERVICE.findAll("", comparatorInner);
        });
        comparator = NameComparator.INSTANCE;
        final List<ProjectDTO> projects = SERVICE.findAll(userId, comparator);
        projects.forEach(project -> Assert.assertEquals(userId, project.getUserId()));
    }

    @Test
    public void findAllSortByUserId() throws Exception {
        @Nullable Sort sort = null;
        Assert.assertNotNull(SERVICE.findAll(userId, sort));
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            @Nullable Sort sortInner = null;
            SERVICE.findAll("", sortInner);
        });
        sort = Sort.BY_NAME;
        final List<ProjectDTO> projects = SERVICE.findAll(userId, sort);
        projects.forEach(project -> Assert.assertEquals(userId, project.getUserId()));
    }

    @Test
    public void findOneById() throws Exception {
        Assert.assertThrows(IdEmptyException.class, () -> SERVICE.findOneById(userId, ""));
        Assert.assertThrows(UserIdEmptyException.class, () -> SERVICE.existsById("", USER_PROJECT1.getId()));
        Assert.assertNull(SERVICE.findOneById(userId, NON_EXISTING_PROJECT_ID));
        @Nullable final ProjectDTO project = SERVICE.findOneById(userId, USER_PROJECT1.getId());
        Assert.assertNotNull(project);
        Assert.assertEquals(USER_PROJECT1.getId(), project.getId());
    }

    @Test
    public void findOneByIdByUserId() throws Exception {
        Assert.assertThrows(IdEmptyException.class, () -> {
            SERVICE.findOneById(userId, "");
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            SERVICE.existsById("", USER_PROJECT1.getId());
        });
        Assert.assertNull(SERVICE.findOneById(userId, NON_EXISTING_PROJECT_ID));
        @Nullable final ProjectDTO project = SERVICE.findOneById(userId, USER_PROJECT1.getId());
        Assert.assertNotNull(project);
        Assert.assertEquals(USER_PROJECT1.getId(), project.getId());
    }

    @Test
    public void getSizeByUserId() throws Exception {
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            SERVICE.getSize("");
        });
        Assert.assertEquals(2, SERVICE.getSize(userId));
    }

    @Before
    public void initTest() throws Exception {
        SERVICE.add(userId, USER_PROJECT1);
        SERVICE.add(userId, USER_PROJECT2);
    }

    @Test
    public void removeByIdByUserId() throws Exception {
        Assert.assertThrows(UserIdEmptyException.class, () -> SERVICE.removeById(null, null));
        Assert.assertThrows(UserIdEmptyException.class, () -> SERVICE.removeById("", null));
        Assert.assertThrows(IdEmptyException.class, () -> SERVICE.removeById(userId, null));
        Assert.assertThrows(IdEmptyException.class, () -> SERVICE.removeById(userId, ""));
        SERVICE.removeById(userId, USER_PROJECT2.getId());
        Assert.assertNull(SERVICE.findOneById(userId, USER_PROJECT2.getId()));
    }

    @Test
    public void removeByUserId() throws Exception {
        SERVICE.remove(userId, USER_PROJECT2);
        Assert.assertNull(SERVICE.findOneById(userId, USER_PROJECT2.getId()));
    }

    @Test
    public void updateById() throws Exception {
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            SERVICE.updateById(null, USER_PROJECT1.getId(), USER_PROJECT1.getName(), USER_PROJECT1.getDescription());
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            SERVICE.updateById("", USER_PROJECT1.getId(), USER_PROJECT1.getName(), USER_PROJECT1.getDescription());
        });
        Assert.assertThrows(IdEmptyException.class, () -> {
            SERVICE.updateById(userId, null, USER_PROJECT1.getName(), USER_PROJECT1.getDescription());
        });
        Assert.assertThrows(IdEmptyException.class, () -> {
            SERVICE.updateById(userId, "", USER_PROJECT1.getName(), USER_PROJECT1.getDescription());
        });
        Assert.assertThrows(NameEmptyException.class, () -> {
            SERVICE.updateById(userId, USER_PROJECT1.getId(), null, USER_PROJECT1.getDescription());
        });
        Assert.assertThrows(NameEmptyException.class, () -> {
            SERVICE.updateById(userId, USER_PROJECT1.getId(), "", USER_PROJECT1.getDescription());
        });
        Assert.assertThrows(DescriptionEmptyException.class, () -> {
            SERVICE.updateById(userId, USER_PROJECT1.getId(), USER_PROJECT1.getName(), null);
        });
        Assert.assertThrows(DescriptionEmptyException.class, () -> {
            SERVICE.updateById(userId, USER_PROJECT1.getId(), USER_PROJECT1.getName(), "");
        });
        @NotNull final String name = USER_PROJECT1.getName() + NON_EXISTING_PROJECT_ID;
        @NotNull final String description = USER_PROJECT1.getDescription() + NON_EXISTING_PROJECT_ID;
        SERVICE.updateById(userId, USER_PROJECT1.getId(), name, description);
        @Nullable final ProjectDTO project = SERVICE.findOneById(userId, USER_PROJECT1.getId());
        Assert.assertNotNull(project);
        Assert.assertEquals(name, project.getName());
        Assert.assertEquals(description, project.getDescription());
    }

}
