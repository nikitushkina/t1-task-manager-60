package ru.t1.nikitushkina.tm.service;

import liquibase.Liquibase;
import liquibase.exception.LiquibaseException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.t1.nikitushkina.tm.api.service.IPropertyService;
import ru.t1.nikitushkina.tm.api.service.dto.IProjectDTOService;
import ru.t1.nikitushkina.tm.api.service.dto.ITaskDTOService;
import ru.t1.nikitushkina.tm.api.service.dto.IUserDTOService;
import ru.t1.nikitushkina.tm.configuration.ServerConfiguration;
import ru.t1.nikitushkina.tm.dto.model.UserDTO;
import ru.t1.nikitushkina.tm.enumerated.Role;
import ru.t1.nikitushkina.tm.exception.field.IdEmptyException;
import ru.t1.nikitushkina.tm.exception.user.*;
import ru.t1.nikitushkina.tm.marker.UnitCategory;
import ru.t1.nikitushkina.tm.migration.AbstractSchemeTest;
import ru.t1.nikitushkina.tm.service.dto.ProjectDTOService;
import ru.t1.nikitushkina.tm.service.dto.TaskDTOService;
import ru.t1.nikitushkina.tm.service.dto.UserDTOService;

import static ru.t1.nikitushkina.tm.constant.UserTestData.*;

@Category(UnitCategory.class)
public final class UserServiceTest extends AbstractSchemeTest {

    @Nullable
    private static IUserDTOService SERVICE;

    @Test
    public void add() throws Exception {
        Assert.assertNotNull(SERVICE.add(ADMIN_TEST));
        @Nullable final UserDTO user = SERVICE.findOneById(ADMIN_TEST.getId());
        Assert.assertNotNull(user);
        Assert.assertEquals(ADMIN_TEST.getId(), user.getId());
    }

    @After
    public void clean() throws Exception {
        @Nullable UserDTO user = SERVICE.findOneById(USER_TEST.getId());
        if (user != null) SERVICE.remove(user);
        user = SERVICE.findByLogin(USER_TEST_LOGIN);
        if (user != null) SERVICE.remove(user);
        user = SERVICE.findOneById(ADMIN_TEST.getId());
        if (user != null) SERVICE.remove(user);
        user = SERVICE.findByLogin(ADMIN_TEST_LOGIN);
        if (user != null) SERVICE.remove(user);
        SERVICE.clear();
    }

    @Test
    public void create() throws Exception {
        Assert.assertThrows(LoginEmptyException.class, () -> {
            SERVICE.create(null, ADMIN_TEST_PASSWORD);
        });
        Assert.assertThrows(LoginEmptyException.class, () -> {
            SERVICE.create("", ADMIN_TEST_PASSWORD);
        });
        Assert.assertThrows(ExistLoginException.class, () -> {
            SERVICE.create(USER_TEST_LOGIN, ADMIN_TEST_PASSWORD);
        });
        Assert.assertThrows(PasswordEmptyException.class, () -> {
            SERVICE.create(ADMIN_TEST_LOGIN, null);
        });
        Assert.assertThrows(PasswordEmptyException.class, () -> {
            SERVICE.create(ADMIN_TEST_LOGIN, "");
        });
        @NotNull final UserDTO user = SERVICE.create(ADMIN_TEST_LOGIN, ADMIN_TEST_PASSWORD);
        Assert.assertEquals(user.getId(), SERVICE.findOneById(user.getId()).getId());
        Assert.assertEquals(ADMIN_TEST_LOGIN, user.getLogin());
        Assert.assertEquals(ADMIN_TEST.getPasswordHash(), user.getPasswordHash());
    }

    @Test
    public void createWithEmail() throws Exception {
        Assert.assertThrows(LoginEmptyException.class, () -> {
            SERVICE.create(null, ADMIN_TEST_PASSWORD, ADMIN_TEST_EMAIL);
        });
        Assert.assertThrows(LoginEmptyException.class, () -> {
            SERVICE.create("", ADMIN_TEST_PASSWORD, ADMIN_TEST_EMAIL);
        });
        Assert.assertThrows(ExistLoginException.class, () -> {
            SERVICE.create(USER_TEST_LOGIN, ADMIN_TEST_PASSWORD, ADMIN_TEST_EMAIL);
        });
        Assert.assertThrows(PasswordEmptyException.class, () -> {
            SERVICE.create(ADMIN_TEST_LOGIN, null, ADMIN_TEST_EMAIL);
        });
        Assert.assertThrows(PasswordEmptyException.class, () -> {
            SERVICE.create(ADMIN_TEST_LOGIN, "", ADMIN_TEST_EMAIL);
        });
        Assert.assertThrows(ExistEmailException.class, () -> {
            SERVICE.create(ADMIN_TEST_LOGIN, ADMIN_TEST_PASSWORD, USER_TEST_EMAIL);
        });
        @NotNull final UserDTO user = SERVICE.create(ADMIN_TEST_LOGIN, ADMIN_TEST_PASSWORD, ADMIN_TEST_EMAIL);
        Assert.assertEquals(user.getId(), SERVICE.findOneById(user.getId()).getId());
        Assert.assertEquals(ADMIN_TEST_LOGIN, user.getLogin());
        Assert.assertEquals(ADMIN_TEST.getPasswordHash(), user.getPasswordHash());
        Assert.assertEquals(ADMIN_TEST_EMAIL, user.getEmail());
    }

    @Test
    public void createWithRole() throws Exception {
        @NotNull final Role role = Role.ADMIN;
        Assert.assertThrows(LoginEmptyException.class, () -> {
            SERVICE.create(null, ADMIN_TEST_PASSWORD, role);
        });
        Assert.assertThrows(LoginEmptyException.class, () -> {
            SERVICE.create("", ADMIN_TEST_PASSWORD, role);
        });
        Assert.assertThrows(ExistLoginException.class, () -> {
            SERVICE.create(USER_TEST_LOGIN, ADMIN_TEST_PASSWORD, role);
        });
        Assert.assertThrows(PasswordEmptyException.class, () -> {
            SERVICE.create(ADMIN_TEST_LOGIN, null, role);
        });
        Assert.assertThrows(PasswordEmptyException.class, () -> {
            SERVICE.create(ADMIN_TEST_LOGIN, "", role);
        });
        Assert.assertThrows(RoleEmptyException.class, () -> {
            @NotNull final Role nullRole = null;
            SERVICE.create(ADMIN_TEST_LOGIN, ADMIN_TEST_PASSWORD, nullRole);
        });
        @NotNull final UserDTO user = SERVICE.create(ADMIN_TEST_LOGIN, ADMIN_TEST_PASSWORD, Role.ADMIN);
        Assert.assertEquals(user.getId(), SERVICE.findOneById(user.getId()).getId());
        Assert.assertEquals(ADMIN_TEST_LOGIN, user.getLogin());
        Assert.assertEquals(ADMIN_TEST.getPasswordHash(), user.getPasswordHash());
        Assert.assertEquals(Role.ADMIN, user.getRole());
    }

    @Test
    public void existsById() throws Exception {
        Assert.assertFalse(SERVICE.existsById(""));
        Assert.assertFalse(SERVICE.existsById(null));
        Assert.assertFalse(SERVICE.existsById(NON_EXISTING_USER_ID));
        Assert.assertTrue(SERVICE.existsById(USER_TEST.getId()));
    }

    @Test
    public void findByLogin() throws Exception {
        Assert.assertThrows(LoginEmptyException.class, () -> {
            SERVICE.findByLogin(null);
        });
        Assert.assertThrows(LoginEmptyException.class, () -> {
            SERVICE.findByLogin("");
        });
        @Nullable final UserDTO user = SERVICE.findByLogin(USER_TEST_LOGIN);
        Assert.assertNotNull(user);
        Assert.assertEquals(USER_TEST.getId(), user.getId());
    }

    @Test
    public void findOneById() throws Exception {
        Assert.assertThrows(IdEmptyException.class, () -> {
            SERVICE.findOneById(null);
        });
        Assert.assertThrows(IdEmptyException.class, () -> {
            SERVICE.findOneById("");
        });
        Assert.assertNull(SERVICE.findOneById(NON_EXISTING_USER_ID));
        @Nullable final UserDTO user = SERVICE.findOneById(USER_TEST.getId());
        Assert.assertNotNull(user);
        Assert.assertEquals(USER_TEST.getId(), user.getId());
    }

    @Before
    public void initTest() throws Exception {
        final Liquibase liquibase = liquibase("changelog/changelog-master.xml");
        liquibase.dropAll();
        liquibase.update("scheme");

        @NotNull final ApplicationContext context = new AnnotationConfigApplicationContext(ServerConfiguration.class);
        SERVICE = context.getBean(IUserDTOService.class);
        SERVICE.add(USER_TEST);
    }

    @Test
    public void isEmailExists() throws Exception {
        Assert.assertFalse(SERVICE.isEmailExists(null));
        Assert.assertFalse(SERVICE.isEmailExists(""));
        Assert.assertTrue(SERVICE.isEmailExists(USER_TEST_EMAIL));
    }

    @Test
    public void isLoginExists() throws Exception {
        Assert.assertFalse(SERVICE.isLoginExists(null));
        Assert.assertFalse(SERVICE.isLoginExists(""));
        Assert.assertTrue(SERVICE.isLoginExists(USER_TEST_LOGIN));
    }

    @Test
    public void lockUserByLogin() throws Exception {
        Assert.assertThrows(LoginEmptyException.class, () -> {
            SERVICE.lockUserByLogin(null);
        });
        Assert.assertThrows(LoginEmptyException.class, () -> {
            SERVICE.lockUserByLogin("");
        });
        SERVICE.lockUserByLogin(USER_TEST_LOGIN);
        @NotNull final UserDTO user = SERVICE.findOneById(USER_TEST.getId());
        Assert.assertTrue(user.getLocked());
    }

    @Test
    public void removeById() throws Exception {
        Assert.assertThrows(IdEmptyException.class, () -> {
            SERVICE.removeById(null);
        });
        Assert.assertThrows(IdEmptyException.class, () -> {
            SERVICE.removeById("");
        });
        SERVICE.add(ADMIN_TEST);
        Assert.assertNotNull(SERVICE.findOneById(ADMIN_TEST.getId()));
        SERVICE.removeById(ADMIN_TEST.getId());
        Assert.assertNull(SERVICE.findOneById(ADMIN_TEST.getId()));
    }

    @Test
    public void removeByLogin() throws Exception {
        Assert.assertThrows(LoginEmptyException.class, () -> {
            SERVICE.removeByLogin(null);
        });
        Assert.assertThrows(LoginEmptyException.class, () -> {
            SERVICE.removeByLogin("");
        });
        SERVICE.add(ADMIN_TEST);
        SERVICE.removeByLogin(ADMIN_TEST_LOGIN);
        Assert.assertNull(SERVICE.findOneById(ADMIN_TEST.getId()));
    }

    @Test
    public void setPassword() throws Exception {
        Assert.assertThrows(IdEmptyException.class, () -> {
            SERVICE.setPassword(null, ADMIN_TEST_PASSWORD);
        });
        Assert.assertThrows(IdEmptyException.class, () -> {
            SERVICE.setPassword("", ADMIN_TEST_PASSWORD);
        });
        Assert.assertThrows(PasswordEmptyException.class, () -> {
            SERVICE.setPassword(USER_TEST.getId(), null);
        });
        Assert.assertThrows(PasswordEmptyException.class, () -> {
            SERVICE.setPassword(USER_TEST.getId(), "");
        });
        SERVICE.setPassword(USER_TEST.getId(), ADMIN_TEST_PASSWORD);
        @NotNull final UserDTO user = SERVICE.findOneById(USER_TEST.getId());
        Assert.assertEquals(ADMIN_TEST.getPasswordHash(), user.getPasswordHash());
        SERVICE.setPassword(USER_TEST.getId(), USER_TEST_PASSWORD);
    }

    @Test
    public void unlockUserByLogin() throws Exception {
        Assert.assertThrows(LoginEmptyException.class, () -> {
            SERVICE.unlockUserByLogin(null);
        });
        Assert.assertThrows(LoginEmptyException.class, () -> {
            SERVICE.unlockUserByLogin("");
        });
        SERVICE.lockUserByLogin(USER_TEST_LOGIN);
        SERVICE.unlockUserByLogin(USER_TEST_LOGIN);
        @NotNull final UserDTO user = SERVICE.findByLogin(USER_TEST_LOGIN);
        Assert.assertFalse(user.getLocked());
    }

    @Test
    public void updateUser() throws Exception {
        @NotNull final String firstName = "User_first_name";
        @NotNull final String lastName = "User_last_name";
        @NotNull final String middleName = "User_middle_name";
        Assert.assertThrows(IdEmptyException.class, () -> {
            SERVICE.updateUser(null, firstName, lastName, middleName);
        });
        Assert.assertThrows(IdEmptyException.class, () -> {
            SERVICE.updateUser("", firstName, lastName, middleName);
        });
        SERVICE.updateUser(USER_TEST.getId(), firstName, lastName, middleName);
        @NotNull final UserDTO user = SERVICE.findOneById(USER_TEST.getId());
        Assert.assertEquals(firstName, user.getFirstName());
        Assert.assertEquals(lastName, user.getLastName());
        Assert.assertEquals(middleName, user.getMiddleName());
    }

}
