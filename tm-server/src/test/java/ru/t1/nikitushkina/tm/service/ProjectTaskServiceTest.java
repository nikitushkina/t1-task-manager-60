package ru.t1.nikitushkina.tm.service;

import liquibase.Liquibase;
import liquibase.exception.LiquibaseException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.t1.nikitushkina.tm.api.service.dto.IProjectDTOService;
import ru.t1.nikitushkina.tm.api.service.dto.IProjectTaskDTOService;
import ru.t1.nikitushkina.tm.api.service.dto.ITaskDTOService;
import ru.t1.nikitushkina.tm.api.service.dto.IUserDTOService;
import ru.t1.nikitushkina.tm.configuration.ServerConfiguration;
import ru.t1.nikitushkina.tm.dto.model.TaskDTO;
import ru.t1.nikitushkina.tm.dto.model.UserDTO;
import ru.t1.nikitushkina.tm.exception.entity.ProjectNotFoundException;
import ru.t1.nikitushkina.tm.exception.entity.TaskNotFoundException;
import ru.t1.nikitushkina.tm.exception.field.ProjectIdEmptyException;
import ru.t1.nikitushkina.tm.exception.field.TaskIdEmptyException;
import ru.t1.nikitushkina.tm.exception.user.UserIdEmptyException;
import ru.t1.nikitushkina.tm.marker.UnitCategory;
import ru.t1.nikitushkina.tm.migration.AbstractSchemeTest;

import static ru.t1.nikitushkina.tm.constant.ProjectTestData.*;
import static ru.t1.nikitushkina.tm.constant.TaskTestData.*;
import static ru.t1.nikitushkina.tm.constant.UserTestData.USER_TEST_LOGIN;
import static ru.t1.nikitushkina.tm.constant.UserTestData.USER_TEST_PASSWORD;

@Category(UnitCategory.class)
public final class ProjectTaskServiceTest extends AbstractSchemeTest {

    @Nullable
    private static IProjectDTOService PROJECT_SERVICE;

    @Nullable
    private static ITaskDTOService TASK_SERVICE;

    @Nullable
    private static IProjectTaskDTOService SERVICE;

    @Nullable
    private static IUserDTOService USER_SERVICE;

    @NotNull
    private static String userId = "";

    @BeforeClass
    public static void setUp() throws Exception {
        final Liquibase liquibase = liquibase("changelog/changelog-master.xml");
        liquibase.dropAll();
        liquibase.update("scheme");

        @NotNull final ApplicationContext context = new AnnotationConfigApplicationContext(ServerConfiguration.class);
        PROJECT_SERVICE = context.getBean(IProjectDTOService.class);
        TASK_SERVICE = context.getBean(ITaskDTOService.class);
        SERVICE = context.getBean(IProjectTaskDTOService.class);
        USER_SERVICE = context.getBean(IUserDTOService.class);
        @NotNull final UserDTO user = USER_SERVICE.create(USER_TEST_LOGIN, USER_TEST_PASSWORD);
        userId = user.getId();
    }

    @AfterClass
    public static void tearDown() throws Exception {
        USER_SERVICE.clear();
    }

    @Before
    public void initTest() throws Exception {
        PROJECT_SERVICE.add(userId, USER_PROJECT1);
        PROJECT_SERVICE.add(userId, USER_PROJECT2);
        TASK_SERVICE.add(userId, USER_TASK1);
        TASK_SERVICE.add(userId, USER_TASK2);
    }

    @After
    public void clean() throws Exception {
        TASK_SERVICE.clear(userId);
        PROJECT_SERVICE.clear(userId);
    }

    @Test
    public void bindTaskToProject() throws Exception {
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            SERVICE.bindTaskToProject(null, USER_PROJECT1.getId(), USER_TASK1.getId());
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            SERVICE.bindTaskToProject("", USER_PROJECT1.getId(), USER_TASK1.getId());
        });
        Assert.assertThrows(ProjectIdEmptyException.class, () -> {
            SERVICE.bindTaskToProject(userId, null, USER_TASK1.getId());
        });
        Assert.assertThrows(ProjectIdEmptyException.class, () -> {
            SERVICE.bindTaskToProject(userId, "", USER_TASK1.getId());
        });
        Assert.assertThrows(TaskIdEmptyException.class, () -> {
            SERVICE.bindTaskToProject(userId, USER_PROJECT1.getId(), null);
        });
        Assert.assertThrows(TaskIdEmptyException.class, () -> {
            SERVICE.bindTaskToProject(userId, USER_PROJECT1.getId(), "");
        });
        Assert.assertThrows(ProjectNotFoundException.class, () -> {
            SERVICE.bindTaskToProject(userId, NON_EXISTING_PROJECT_ID, USER_TASK1.getId());
        });
        Assert.assertThrows(TaskNotFoundException.class, () -> {
            SERVICE.bindTaskToProject(userId, USER_PROJECT1.getId(), NON_EXISTING_TASK_ID);
        });
        SERVICE.bindTaskToProject(userId, USER_PROJECT2.getId(), USER_TASK1.getId());
        @Nullable final TaskDTO task = TASK_SERVICE.findOneById(userId, USER_TASK1.getId());
        Assert.assertEquals(USER_PROJECT2.getId(), task.getProjectId());
        SERVICE.bindTaskToProject(userId, USER_PROJECT1.getId(), USER_TASK1.getId());
    }

    @Test
    public void removeProjectById() throws Exception {
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            SERVICE.removeProjectById(null, USER_PROJECT1.getId());
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            SERVICE.removeProjectById("", USER_PROJECT1.getId());
        });
        Assert.assertThrows(ProjectIdEmptyException.class, () -> {
            SERVICE.removeProjectById(userId, null);
        });
        Assert.assertThrows(ProjectIdEmptyException.class, () -> {
            SERVICE.removeProjectById(userId, "");
        });
        Assert.assertThrows(ProjectNotFoundException.class, () -> {
            SERVICE.removeProjectById(userId, NON_EXISTING_PROJECT_ID);
        });
        SERVICE.bindTaskToProject(userId, USER_PROJECT1.getId(), USER_TASK1.getId());
        SERVICE.bindTaskToProject(userId, USER_PROJECT1.getId(), USER_TASK2.getId());
        SERVICE.removeProjectById(userId, USER_PROJECT1.getId());
        Assert.assertNull(PROJECT_SERVICE.findOneById(userId, USER_PROJECT1.getId()));
        Assert.assertNull(TASK_SERVICE.findOneById(userId, USER_TASK1.getId()));
        Assert.assertNull(TASK_SERVICE.findOneById(userId, USER_TASK2.getId()));
    }

    @Test
    public void unbindTaskFromProject() throws Exception {
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            SERVICE.unbindTaskFromProject(null, USER_PROJECT1.getId(), USER_TASK1.getId());
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            SERVICE.unbindTaskFromProject("", USER_PROJECT1.getId(), USER_TASK1.getId());
        });
        Assert.assertThrows(ProjectIdEmptyException.class, () -> {
            SERVICE.unbindTaskFromProject(userId, null, USER_TASK1.getId());
        });
        Assert.assertThrows(ProjectIdEmptyException.class, () -> {
            SERVICE.unbindTaskFromProject(userId, "", USER_TASK1.getId());
        });
        Assert.assertThrows(TaskIdEmptyException.class, () -> {
            SERVICE.unbindTaskFromProject(userId, USER_PROJECT1.getId(), null);
        });
        Assert.assertThrows(TaskIdEmptyException.class, () -> {
            SERVICE.unbindTaskFromProject(userId, USER_PROJECT1.getId(), "");
        });
        Assert.assertThrows(ProjectNotFoundException.class, () -> {
            SERVICE.unbindTaskFromProject(userId, NON_EXISTING_PROJECT_ID, USER_TASK1.getId());
        });
        Assert.assertThrows(TaskNotFoundException.class, () -> {
            SERVICE.unbindTaskFromProject(userId, USER_PROJECT1.getId(), NON_EXISTING_TASK_ID);
        });
        SERVICE.unbindTaskFromProject(userId, USER_PROJECT1.getId(), USER_TASK1.getId());
        @Nullable final TaskDTO task = TASK_SERVICE.findOneById(userId, USER_TASK1.getId());
        Assert.assertNull(task.getProjectId());
        SERVICE.bindTaskToProject(userId, USER_PROJECT1.getId(), USER_TASK1.getId());
    }

}
