package ru.t1.nikitushkina.tm.service;

import liquibase.Liquibase;
import liquibase.exception.LiquibaseException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.t1.nikitushkina.tm.api.service.dto.ISessionDTOService;
import ru.t1.nikitushkina.tm.api.service.dto.IUserDTOService;
import ru.t1.nikitushkina.tm.configuration.ServerConfiguration;
import ru.t1.nikitushkina.tm.dto.model.SessionDTO;
import ru.t1.nikitushkina.tm.dto.model.UserDTO;
import ru.t1.nikitushkina.tm.exception.field.IdEmptyException;
import ru.t1.nikitushkina.tm.exception.user.UserIdEmptyException;
import ru.t1.nikitushkina.tm.marker.UnitCategory;
import ru.t1.nikitushkina.tm.migration.AbstractSchemeTest;

import java.util.List;

import static ru.t1.nikitushkina.tm.constant.SessionTestData.*;
import static ru.t1.nikitushkina.tm.constant.UserTestData.USER_TEST_LOGIN;
import static ru.t1.nikitushkina.tm.constant.UserTestData.USER_TEST_PASSWORD;

@Category(UnitCategory.class)
public final class SessionServiceTest extends AbstractSchemeTest {

    @Nullable
    private static IUserDTOService USER_SERVICE;

    @Nullable
    private static ISessionDTOService SERVICE;

    @NotNull
    private static String userId = "";

    @BeforeClass
    public static void setUp() throws Exception {
        final Liquibase liquibase = liquibase("changelog/changelog-master.xml");
        liquibase.dropAll();
        liquibase.update("scheme");

        @NotNull final ApplicationContext context = new AnnotationConfigApplicationContext(ServerConfiguration.class);
        SERVICE = context.getBean(ISessionDTOService.class);
        USER_SERVICE = context.getBean(IUserDTOService.class);
        @NotNull final UserDTO user = USER_SERVICE.create(USER_TEST_LOGIN, USER_TEST_PASSWORD);
        userId = user.getId();
    }

    @AfterClass
    public static void tearDown() throws Exception {
        USER_SERVICE.clear();
    }

    @Test
    public void addByUserId() throws Exception {
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            SERVICE.add(null, USER_SESSION3);
        });
        Assert.assertNotNull(SERVICE.add(userId, USER_SESSION3));
        @Nullable final SessionDTO session = SERVICE.findOneById(userId, USER_SESSION3.getId());
        Assert.assertNotNull(session);
        Assert.assertEquals(USER_SESSION3.getId(), session.getId());
    }

    @After
    public void clean() throws Exception {
        SERVICE.clear(userId);
    }

    @Test
    public void clearByUserId() throws Exception {
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            SERVICE.clear("");
        });
        SERVICE.clear(userId);
        Assert.assertEquals(0, SERVICE.getSize(userId));
    }

    @Test
    public void existsById() throws Exception {
        Assert.assertThrows(UserIdEmptyException.class, () -> SERVICE.existsById("", NON_EXISTING_SESSION_ID));
        Assert.assertFalse(SERVICE.existsById(userId, ""));
        Assert.assertFalse(SERVICE.existsById(userId, NON_EXISTING_SESSION_ID));
        Assert.assertTrue(SERVICE.existsById(userId, USER_SESSION1.getId()));
    }

    @Test
    public void existsByIdByUserId() throws Exception {
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            SERVICE.existsById("", NON_EXISTING_SESSION_ID);
        });
        Assert.assertFalse(SERVICE.existsById(userId, ""));
        Assert.assertFalse(SERVICE.existsById(userId, NON_EXISTING_SESSION_ID));
        Assert.assertTrue(SERVICE.existsById(userId, USER_SESSION1.getId()));
    }

    @Test
    public void findAllByUserId() throws Exception {
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            SERVICE.findAll("");
        });
        final List<SessionDTO> sessions = SERVICE.findAll(userId);
        Assert.assertEquals(2, sessions.size());
        sessions.forEach(session -> Assert.assertEquals(userId, session.getUserId()));
    }

    @Test
    public void findOneById() throws Exception {
        Assert.assertThrows(IdEmptyException.class, () -> {
            SERVICE.findOneById(userId, "");
        });
        Assert.assertNull(SERVICE.findOneById(userId, NON_EXISTING_SESSION_ID));
        @Nullable final SessionDTO session = SERVICE.findOneById(userId, USER_SESSION1.getId());
        Assert.assertNotNull(session);
        Assert.assertEquals(USER_SESSION1.getId(), session.getId());
    }

    @Test
    public void findOneByIdByUserId() throws Exception {
        Assert.assertThrows(IdEmptyException.class, () -> {
            SERVICE.findOneById(userId, "");
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            SERVICE.existsById("", USER_SESSION1.getId());
        });
        Assert.assertNull(SERVICE.findOneById(userId, NON_EXISTING_SESSION_ID));
        @Nullable final SessionDTO session = SERVICE.findOneById(userId, USER_SESSION1.getId());
        Assert.assertNotNull(session);
        Assert.assertEquals(USER_SESSION1.getId(), session.getId());
    }

    @Test
    public void getSizeByUserId() throws Exception {
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            SERVICE.getSize("");
        });
        Assert.assertEquals(2, SERVICE.getSize(userId));
    }

    @Before
    public void initTest() throws Exception {
        SERVICE.add(userId, USER_SESSION1);
        SERVICE.add(userId, USER_SESSION2);
    }

    @Test
    public void removeByIdByUserId() throws Exception {
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            SERVICE.removeById(null, null);
        });
        Assert.assertThrows(UserIdEmptyException.class, () -> {
            SERVICE.removeById("", null);
        });
        Assert.assertThrows(IdEmptyException.class, () -> {
            SERVICE.removeById(userId, null);
        });
        Assert.assertThrows(IdEmptyException.class, () -> {
            SERVICE.removeById(userId, "");
        });
        SERVICE.removeById(userId, USER_SESSION2.getId());
        Assert.assertNull(SERVICE.findOneById(userId, USER_SESSION2.getId()));
    }

    @Test
    public void removeByUserId() throws Exception {
        SERVICE.remove(userId, USER_SESSION2);
        Assert.assertNull(SERVICE.findOneById(userId, USER_SESSION2.getId()));
    }

}
