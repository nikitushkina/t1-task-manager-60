package ru.t1.nikitushkina.tm.repository.dto;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.t1.nikitushkina.tm.api.repository.dto.ISessionDTORepository;
import ru.t1.nikitushkina.tm.dto.model.SessionDTO;

@Repository
@Scope("prototype")
@NoArgsConstructor
public class SessionDTORepository extends AbstractUserOwnedDTORepository<SessionDTO>
        implements ISessionDTORepository {

    @NotNull
    @Override
    protected Class<SessionDTO> getClazz() {
        return SessionDTO.class;
    }

}
