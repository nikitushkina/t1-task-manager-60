package ru.t1.nikitushkina.tm.service;

import liquibase.Liquibase;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.t1.nikitushkina.tm.api.service.IAdminService;
import ru.t1.nikitushkina.tm.api.service.IPropertyService;
import ru.t1.nikitushkina.tm.exception.user.AccessDeniedException;

@Service
@NoArgsConstructor
@AllArgsConstructor
public class AdminService implements IAdminService {

    @Getter
    @NotNull
    @Autowired
    protected Liquibase liquibase;
    @NotNull
    @Autowired
    private IPropertyService propertyService;
    @Nullable
    @Autowired
    private IAdminService repository;

    @Override
    @SneakyThrows
    public void dropScheme(@Nullable String initToken) {
        @NotNull final String token = propertyService.getTokenInit();
        if (initToken == null || !initToken.equals(token)) throw new AccessDeniedException();
        @NotNull final IAdminService repository = getRepository();
        @NotNull final Liquibase liquibase = repository.getLiquibase();
        liquibase.dropAll();
        liquibase.close();
    }

    @NotNull
    protected IAdminService getRepository() {
        return repository;
    }

    @Override
    @SneakyThrows
    public void initScheme(@Nullable String initToken) {
        @NotNull final String token = propertyService.getTokenInit();
        if (initToken == null || !initToken.equals(token)) throw new AccessDeniedException();
        @NotNull final IAdminService repository = getRepository();
        @NotNull final Liquibase liquibase = repository.getLiquibase();
        liquibase.update("scheme");
        liquibase.close();
    }

}
